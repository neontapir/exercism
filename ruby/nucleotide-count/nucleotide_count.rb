class Nucleotide
  def initialize(strand)
    @strand = strand
  end

  def self.from_dna(strand)
    raise ArgumentError if strand.count('^ATCG') > 0
    Nucleotide.new(strand)
  end

  EMPTY = { 'A' => 0, 'T' => 0, 'C' => 0, 'G' => 0 }
  def histogram
    frequency = EMPTY.clone
    @strand.chars.each do |nucleotide|
      frequency[nucleotide] += 1
    end
    frequency
  end

  def count(nucleotide)
    histogram[nucleotide]
  end
end
