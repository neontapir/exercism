class Fixnum
  VERSION = 1

  SYMBOLS = {
    1000 => 'M', 900 => 'CM',
    500 => 'D', 400 => 'CD',
    100 => 'C', 90 => 'XC',
    50 => 'L', 40 => 'XL',
    10 => 'X', 9 => 'IX',
    5 => 'V', 4 => 'IV',
    1 => 'I'
  }

  def to_roman
    value = self
    roman = ""

    while (value > 0) do
      SYMBOLS.each do |number, symbol|
        if (value >= number) then
          roman += symbol
          value -= number
          break
        end
      end
    end

    roman
  end
end
