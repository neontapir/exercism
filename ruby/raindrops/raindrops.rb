class Raindrops
  VERSION = 1
  SOUNDS = { 3 => "Pling", 5 => "Plang", 7 => "Plong" }

  def self.convert(drops)
    sound = ""

    SOUNDS.each do |prime, tone|
      sound += tone if drops % prime == 0
    end

    sound != "" ? sound : drops.to_s
  end
end
