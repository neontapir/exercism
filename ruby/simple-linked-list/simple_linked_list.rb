module NilClassRefinements
  refine NilClass do
    def reverse
      nil
    end
  end
end

class Element
  attr_reader :datum, :next_item

  def initialize(datum, next_item)
    @datum, @next_item = datum, next_item
  end

  def next
    @next_item
  end

  def reverse
    Element.from_a(to_a.reverse)
  end

  def self.from_a(array)
    values = array.to_a
    next_element = nil
    while (! values.empty?) do
      item = values.pop
      next_element = Element.new(item, next_element)
    end
    next_element
  end

  def from_a
    Element.from_a(self)
  end

  def self.to_a(item)
    result = []
    unless (item.nil?)
      result << item.datum
      unless (item.next.nil?)
        result << to_a(item.next)
      end
    end
    result.flatten
  end

  def to_a
    Element.to_a(self)
  end
end
