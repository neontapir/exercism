class School
  VERSION = 1
  attr_reader :students

  def initialize()
    @students = Hash.new{|hash, key| hash[key] = Array.new}
  end

  def add(name, grade)
    @students[grade] = (@students[grade] << name).sort
  end

  def grade(grade)
    @students[grade]
  end

  def to_h
    @students.sort
  end
end
