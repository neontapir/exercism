class Series
  VERSION = 1

  def initialize(series)
    raise ArgumentError if series.count('^0-9') > 0
    @series = series
  end

  def largest_product(digits)
    raise ArgumentError unless digits > -1 and digits <= @series.length
    return 1 if digits == 0
    @series.chars.
      map(&:to_i).
      each_cons(digits).
      map{|group| group.reduce(1, &:*)}.
      max
  end

  def chunks(digits)
    @series.chars.map(&:to_i).each_cons(digits)
  end
end
