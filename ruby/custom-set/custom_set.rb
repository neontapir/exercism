class CustomSet
  attr_reader :values

  def initialize(values = [])
    @values = values
  end

  def delete(value)
    @values.
      select{|x| x.class == value.class and x == value}.
      each{|x| @values.delete(x) }
    self
  end

  def union(other)
    update = self
    other.values.each do |x|
      update.put(x)
    end
    update
  end

  def intersection(other)
    CustomSet.new(@values & other.values)
  end

  def difference(other)
    CustomSet.new(@values - other.values)
  end

  def disjoint?(other)
    (@values - other.values) == @values
  end

  def member?(value)
    @values.any?{|x| x.class == value.class and x == value}
  end

  def subset?(other)
    other.equal? intersection(other)
  end

  def put(value)
    update = @values
    update << value
    CustomSet.new(update)
  end

  def size
    @values.uniq.size
  end

  def empty
    CustomSet.new
  end

  def to_a
    @values.uniq
  end

  def ==(other)
    equal? other
  end

  def equal?(other)
    @values.uniq.sort == other.values.uniq.sort
  end
end
