class Queens
  require_relative 'chessboard'

  def initialize(positions = {})
    @positions = positions
    raise ArgumentError, "Cannot occupy same space" if black == white
  end

  def black
    @positions[:black] or [7, 3]
  end

  def white
    @positions[:white] or [0, 3]
  end

  def attack?
    same_row? or same_column? or on_diagonal?
  end

  def to_s
    Chessboard.new(black, white).to_s
  end

  private

  def same_row?
    black[0] == white[0]
  end

  def same_column?
    black[1] == white[1]
  end

  def on_diagonal?
    (black[1] - white[1]).abs == (black[0] - white[0]).abs
  end
end
