class Chessboard
  def initialize(black, white)
    @board = Array.new(8) { Array.new(8, '_') }
    @board[black[0]][black[1]] = 'B'
    @board[white[0]][white[1]] = 'W'
  end

  def to_s
    result = ''
    (0..7).each do |row|
      result << format("%s\n", @board[row].join(' '))
    end
    result.chomp
  end
end
