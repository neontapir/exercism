class Garden
  STUDENTS = %w(Alice Bob Charlie David Eve Fred
  Ginny Harriet Ileana Joseph Kincaid Larry)

  PLANTS = {
    'G' => :grass, 'C' => :clover,
    'R' => :radishes, 'V' => :violets
  }

  def initialize(diagram, students = STUDENTS)
    @diagram = diagram
    @students = students.map(&:downcase).sort
  end

  private

  def plants_for(name)
    position = 2 * @students.find_index(name)
    @diagram.split("\n").reduce([]) do |sum, row|
      sum << PLANTS[row[position]] << PLANTS[row[position+1]]
    end
  end

  def method_missing(method_id)
    name = method_id.to_s
    super unless @students.include? name
    plants_for(name)
  end
end
