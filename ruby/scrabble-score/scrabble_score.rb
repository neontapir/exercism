module ArrayRefinements
  refine Array do
    def contains?(value)
      self.any?{|x| x == value}
    end
  end
end

class Scrabble
  using ArrayRefinements

  WORTH = {
    ['A', 'E', 'I', 'O', 'U', 'L', 'N', 'R', 'S', 'T'] => 1,
    ['D', 'G'] => 2,
    ['B', 'C', 'M', 'P'] => 3,
    ['F', 'H', 'V', 'W', 'Y'] => 4,
    ['K'] => 5,
    ['J', 'X'] => 8,
    ['Q', 'Z'] => 10,
  }

  def initialize(word)
    @letters = word.to_s.upcase.tr('^A-Z', '')
  end

  def score
    @letters.
      chars.
      map{|letter| WORTH.keys.find {|set| set.contains? letter}}.
      map{|set| WORTH[set]}.
      reduce(0, &:+)
  end

  def self.score(word)
    new(word).score
  end
end
