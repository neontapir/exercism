class CircularBuffer
  class BufferEmptyException < Exception
  end

  class BufferFullException < Exception
  end

  def initialize(size)
    @items = Array.new(size)
    @read_position = 0
    @write_position = 0
  end

  def read
    #puts "read '#{@items[@read_position]}' from buffer pos #{@read_position+1}"
    result = @items[@read_position] or raise BufferEmptyException
    @items[@read_position] = nil
    @read_position = new_position(@read_position)
    result
  end

  def write(value, overwrite = false)
    raise BufferFullException unless overwrite or
      value.nil? or
      @items[@write_position].nil?
    @items[@write_position] = value
    @write_position = new_position(@write_position) unless value.nil?
    #puts self
  end

  def write!(value)
    write(value, true)
    @read_position = new_position(@read_position) unless value.nil?
  end

  def clear
    @items = @items.map { |i| nil }
  end

  private

  def new_position(old_position)
    (old_position + 1) % @items.size
  end

  def to_s
    "buffer:#{@items}{r:#{@read_position+1},w:#{@write_position+1}}"
  end
end
