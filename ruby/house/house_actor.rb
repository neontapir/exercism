class HouseActor
  attr_reader :name, :fate

  def initialize(argument)
    @name = argument[:name]
    @fate = argument[:fate]
  end

  def verse(actors)
    actors.reverse.
      inject(["This is the #{@name}"]) { |sum, a| sum << "that #{a.fate} the #{a.name}" }.
      to_a.join("\n")
  end

  def previous(actors)
    actors.take_while{|a| a != self }
  end

  def ==(other)
    eql? other
  end

  def eql?(other)
    @name == other.name and @fate == other.fate
  end

  def hash
    @name.hash ^ @fate.hash
  end
end
