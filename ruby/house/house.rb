class House
  require_relative 'house_actor'

  DATA = [
    {name: 'house that Jack built.', fate: 'lay in'},
    {name: 'malt', fate: 'ate'},
    {name: 'rat', fate: 'killed'},
    {name: 'cat', fate: 'worried'},
    {name: 'dog', fate: 'tossed'},
    {name: 'cow with the crumpled horn', fate: 'milked'},
    {name: 'maiden all forlorn', fate: 'kissed'},
    {name: 'man all tattered and torn', fate: 'married'},
    {name: 'priest all shaven and shorn', fate: 'woke'},
    {name: 'rooster that crowed in the morn', fate: 'kept'},
    {name: 'farmer sowing his corn', fate: 'belonged to'},
    {name: 'horse and the hound and the horn'},
  ]

  def self.recite
    actors = DATA.map { |d| HouseActor.new(d) }
    actors.map { |actor| actor.verse actor.previous(actors) }.
      to_a.
      join("\n\n") + "\n"
  end
end
