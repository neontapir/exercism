# added 'using ArrayRefinements' to class ArrayTest

module ArrayRefinements
  refine Array do
    def accumulate
      result = []
      self.each do |x|
        result << yield(x)
      end
      result
    end
  end
end
