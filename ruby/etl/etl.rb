class ETL
  def self.transform(input)
    input.each_with_object({}) do |(key, value), result|
      value.each {|x| result[x.downcase] = key }
    end
  end
end
