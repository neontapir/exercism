class Hamming
  VERSION = 1

  def self.compute(first, second)
    raise ArgumentError, 'Sequences must be of equal length' if first.length != second.length
    hamming = 0

    for pos in 0..first.length - 1
      hamming += 1 unless first[pos].chr == second[pos].chr
    end
      
    hamming
  end
end
