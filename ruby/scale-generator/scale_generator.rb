module StringRefinements
  refine String do
    def titleize
      self[0, 1].upcase + self[1..-1]
    end
  end
end

class Scale
  using StringRefinements

  def initialize(tonic, scale_type, intervals = 'mmmmmmmmmmmm')
    @tonic, @scale_type, @intervals = tonic, scale_type, intervals
  end

  SHARP_CHROMATIC = %w(A A# B C C# D D# E F F# G G#)
  FLAT_CHROMATIC = %w(A Bb B C Db D Eb E F Gb G Ab)

  def name
    "#{tonic} #{@scale_type}"
  end

  def tonic
    @tonic.titleize
  end

  def pitches
    array = tones
    array.rotate!(array.index(tonic))
    result = []
    @intervals.each_char do |step|
      result << array[0]
      array.rotate!(step_amount(step))
    end
    result
  end

  def tones
    if (major_naming_rules? and %w(F Bb Eb Ab Db Gb).include? tonic) or
      (not major_naming_rules? and %w(D G C F Bb Eb).include? tonic)
      FLAT_CHROMATIC
    else
      SHARP_CHROMATIC
    end
  end

  def major_naming_rules?
    [:chromatic, :major, :octatonic, :hexatonic, :enigma].include? @scale_type
  end

  def step_amount(step_code)
    if step_code == 'A'
      3
    elsif step_code == 'M'
      2
    else
      1
    end
  end
end
