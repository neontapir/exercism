class Anagram
  attr_reader :word
  def initialize(word)
    @word = word
  end

  def match(list)
    lowercase_word = @word.downcase
    sorted = lowercase_word.chars.sort
    list.
      reject{ |w| w.downcase == lowercase_word }.
      select{ |w| w.downcase.chars.sort == sorted}
  end
end
