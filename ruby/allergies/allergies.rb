class Allergies
  ALLERGENS = {
    :eggs => 1,
    :peanuts => 2,
    :shellfish => 4,
    :strawberries => 8,
    :tomatoes => 16,
    :chocolate => 32,
    :pollen => 64,
    :cats => 128,
  }

  def initialize(allergies)
    @allergies = allergies
  end

  def allergic_to?(item)
    @allergies & ALLERGENS[item.to_sym] > 0
  end

  def list
    ALLERGENS.keys.
      select{|k| allergic_to? k}.
      map{|k| k.to_s}
  end
end
