class Meetup
  def initialize(month, year)
    @month = month
    @year = year
  end

  DAYS = [:sunday, :monday, :tuesday, :wednesday, :thursday, :friday, :saturday]
  PERIODS = [:first, :second, :third, :fourth, :last, :teenth]

  def day(weekday, period)
    matches = matching_dates(weekday)
    Date.new(@year, @month, matches[period])
  end

  private

  def matching_dates(weekday)
    day1 = Date.new(@year, @month, 1)
    last = Date.new(@year, @month, -1)
    days_offset = ((DAYS.index(weekday) - day1.wday) % 7) + 1

    matches = Hash.new(-1)
    position = 0
    (days_offset..(last.day)).step(7) do |day|
      matches[PERIODS[position]] = day
      position += 1
    end
    matches[:last] = matches[:fourth] unless matches[:last] > 0
    matches[:teenth] = matches.find{|k,v| (13..19).include? v }.last
    matches
  end
end
