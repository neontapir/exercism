class SpaceAge
  attr_reader :age

  SECONDS_PER_YEAR = 31_557_600
  PLANETS = {
    :mercury => 0.2408467,
    :venus => 0.61519726,
    :earth => 1.0,
    :mars => 1.8808158,
    :jupiter => 11.862615,
    :saturn => 29.447498,
    :uranus => 84.016846,
    :neptune => 164.79132
  }

  def initialize(age_in_seconds)
    @age = age_in_seconds
  end

  def seconds
    @age
  end

  def on_planet(name)
     @age / (PLANETS[name] * SECONDS_PER_YEAR)
  end

  PLANETS.keys.each do |planet|
    define_method("on_#{planet}") do
      on_planet planet
    end
  end
end
