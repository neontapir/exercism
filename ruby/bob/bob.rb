class Bob
  def hey(remark)
    case
    when shouting?(remark) then 'Whoa, chill out!'
    when question?(remark) then 'Sure.'
    when silence?(remark) then 'Fine. Be that way!'
    else 'Whatever.'
    end
  end

  private

  def shouting?(remark)
    !!(remark =~ /[A-Z]/m && remark !~ /[a-z]/m)
  end

  def question?(remark)
    remark[-1] == '?'
  end

  def silence?(remark)
    !!(remark !~ /\w/m)
  end
end
