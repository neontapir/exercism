class Acronym
  VERSION = 1

  def self.abbreviate(phrase)
    phrase.
      gsub(/([a-z])([A-Z])/, '\1  \2').
      tr(':-', ' ').
      split(' ').
      map{|string| string[0]}.
      join.
      upcase
  end
end
