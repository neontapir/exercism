class Deque
  attr_reader :first, :last

  Element = Struct.new(:datum, :prev, :next)

  def push(datum)
    new1 = Element.new(datum)
    unless @last.nil?
      new1.prev = @last
      @last.next = new1
    end
    @last = new1
    @first ||= new1
  end

  def unshift(datum)
    new1 = Element.new(datum)
    unless @first.nil?
      new1.next = @first
      @first.prev = new1
    end
    @first = new1
    @last ||= new1
  end

  def pop
    result = @last.datum
    @last = @last.prev
    result
  end

  def shift
    result = @first.datum
    @first = @first.next
    result
  end
end
