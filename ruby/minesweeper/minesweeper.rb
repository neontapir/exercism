class ValueError < Exception
end

class Board
  def self.transform(input)
    grid = self.create_grid(input)
    annotated = self.annotate_grid(grid)
    format_grid(annotated)
  end

  private

  def self.create_grid(input)
    grid = Hash.new{|hash, key| hash[key] = Array.new}
    columns = 0
    input.each_with_index do |row, index|
      the_row = row.chars.to_a
      columns = the_row.length if columns == 0
      raise ValueError if columns != the_row.length
      grid[index] = the_row
    end
    raise ValueError unless border_intact? grid and valid_contents? grid
    grid
  end

  def self.border_intact?(grid)
    first(grid).join.count('^+-') == last(grid).join.count('^+-') and
      grid.values.all? {|row| first(row).count('+|') == last(row).count('+|') }
  end

  def self.first(array)
    array[0]
  end

  def self.last(array)
    array[array.length-1]
  end

  def self.valid_contents?(grid)
    grid.values.all? {|row| row.join.count('^+|*123456789 -') == 0 }
  end

  def self.annotate_grid(grid)
    result = grid
    grid.values.each_with_index do |row, r|
      row.each_with_index do |value, c|
        result[r][c] = value
        if value == '*'
          result = self.mark_mine(result, r, c)
        end
      end
    end
    result
  end

  def self.mark_mine(grid, row, column)
    for r in row-1..row+1
      for c in column-1..column+1
        if grid[r][c].count('1-9') > 0
          grid[r][c] = grid[r][c].next!
        elsif grid[r][c].count('+|*-') == 0
          grid[r][c] = '1'
        end
      end
    end
    grid
  end

  def self.format_grid(grid)
    grid.values.map{|row| row.join}
  end
end
