class BeerSong
  def verse(pints)
    beer_left = subtract_one(pints)
    result = "#{quantifier_term(pints).to_s.capitalize} bottle#{beer_plural(pints)} of beer on the wall, " \
             "#{quantifier_term(pints)} bottle#{beer_plural(pints)} of beer.\n" \
             "#{action_phrase(pints)}" \
             "#{quantifier_term(beer_left)} bottle#{beer_plural(beer_left)} of beer on the wall.\n"
    result
  end

  def verses(first, last)
    first.downto(last).inject("") do |sum,x|
      sum << "#{verse(x)}\n"
    end
  end

  def sing
    verses(99, 0)
  end

  private

  def subtract_one(pints)
    pints == 0 ? 99 : pints - 1
  end

  def quantifier_term(pints)
    pints == 0 ? "no more" : pints
  end

  def action_phrase(pints)
    pints == 0 ? "Go to the store and buy some more, " : "Take #{removal_word(pints)} down and pass it around, "
  end

  def beer_plural(pints)
    pints == 1 ? "" : "s"
  end

  def removal_word(pints)
    pints == 1 ? "it" : "one"
  end
end
