class Atbash
  ALPHABET = ('a'..'z').to_a
  DIGITS = ('0'..'9')
  CIPHER = Hash[ALPHABET.zip ALPHABET.reverse].
      merge!(Hash[DIGITS.zip DIGITS])

  def self.encode(input)
    result = input.
              downcase.
              tr('^0-9a-z','').
              chars.
              map{|c| CIPHER[c]}.
              each_slice(5).
              to_a.
              map{|a| a.join}.
              join(' ')
  end
end
