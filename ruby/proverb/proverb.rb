class Proverb
  def initialize(*words, qualifier: nil)
    @words = words
    @qualifier = qualifier
  end

  def to_s
    result = @words.each_cons(2).
      inject([]) {|sum, x| sum << "For want of a #{x[0]} the #{x[1]} was lost."}
    result << "And all for the want of a #{@qualifier}" +
      "#{' ' unless @qualifier.nil?}#{@words[0]}."
    result.join("\n")
  end
end
