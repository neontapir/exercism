class Sieve
  attr_reader :bound

  def initialize(bound)
    @bound = bound
  end

  def primes
    candidates = (0..bound).to_a
    prime = 2
    until prime.nil? do
      multiples = ((prime * prime)..bound)
      multiples.step(prime).each do |multiple|
        candidates[multiple] = 0
      end
      prime = candidates.find {|n| candidates[n] > prime}
    end
    candidates.select {|n| n > 1}
  end
end
