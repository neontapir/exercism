class RunLengthEncoding
  def self.encode(input)
    input.chars
      .slice_when{ |before, after| before != after }
      .map { |s| "#{s.size if s.size > 1}#{s.first}" }
      .join
  end

  def self.decode(input)
    input.chars
      .slice_when{ |before, after| before =~ /[[[:alpha:]]|[[:space:]]]/ }
      .map { |s| "#{s.last}" * (s.size > 1 ? s[0..-2].join.to_i : 1)}
      .join
  end
end

module BookKeeping
  VERSION = 3
end
