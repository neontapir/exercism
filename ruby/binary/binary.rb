class Binary
  VERSION = 1
  attr_reader :binary

  def initialize(binary)
    raise ArgumentError if binary.count('^01') > 0
    @binary = binary
  end

  def to_decimal
    place = 0
    @binary.reverse.chars.inject(0) do |sum, d|
      place += 1
      sum += d.to_i * 2**(place-1)
    end
  end
end
