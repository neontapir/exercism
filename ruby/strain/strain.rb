# also put 'using ArrayRefinements' within ArrayTest class

module ArrayRefinements
  refine Array do
    def keep(&block)
      strain(true, &block)
    end

    def discard(&block)
      strain(false, &block)
    end

    private

    def strain(expected_result, &block)
      result = []
      self.each do |x|
        result << x if block.call(x) == expected_result
      end
      result
    end
  end
end
