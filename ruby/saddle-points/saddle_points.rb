class Matrix
  def initialize(values)
    @matrix = values.split(/\n/).reduce([]) do |sum, row|
      sum << row.split(' ').map(&:to_i)
    end
  end

  def rows
    @matrix
  end

  def columns
    @matrix.transpose
  end

  def saddle_points
    result = []
    rows.each_index do |r|
      @matrix[r].each_index do |c|
        me = @matrix[r][c]
        result << [r,c] if me == rows[r].max and me == columns[c].min
      end
    end
    result
  end
end
