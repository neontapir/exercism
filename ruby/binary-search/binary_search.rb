class BinarySearch
  attr_reader :list

  def initialize(list)
    raise ArgumentError if list.sort != list
    @list = list
  end

  def search_for(item, first = 0, last = list.length)
    mid = middle(first, last)
    return mid if item == list[mid]
    if first < last
      return search_for(item, first, mid-1) if item < list[mid]
      return search_for(item, mid+1, last) if item > list[mid]
    end
    raise RuntimeError
  end

  def middle(first = 0, last = list.length)
    first + ((last - first) / 2)
  end
end
