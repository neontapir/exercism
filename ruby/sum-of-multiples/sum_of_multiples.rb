class SumOfMultiples
  attr_reader :factors

  def initialize(*factors)
    @factors = factors
  end

  def self.to(bound)
    SumOfMultiples.new(3, 5).to(bound)
  end

  def to(bound)
    multiples_of(@factors, bound).reduce(:+) or 0
  end

  private

  def multiples_of(set, bound)
    (1...bound).select do |x|
      set.any? { |s| x % s == 0 }
    end
  end
end
