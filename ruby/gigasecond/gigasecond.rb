class Gigasecond
  VERSION = 1

  def self.from(birth)
    birth + 10**9    
  end
end
