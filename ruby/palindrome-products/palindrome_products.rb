class Palindromes
  require_relative 'palindrome'
  
  def initialize(configuration)
    @min = configuration[:min_factor] || 1
    @max = configuration[:max_factor]
  end

  def generate
    @palindromes = ((@min**2)..(@max**2)).
      select{|n| Palindrome.is? n }.
      map{|p| Palindrome.new(p, @min, @max)}.
      reject{|p| p.factors.empty? }
  end

  def largest
    @palindromes[-1]
  end

  def smallest
    @palindromes[0]
  end
end
