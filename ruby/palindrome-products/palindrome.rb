class Palindrome
  def initialize(number, min, max)
    raise ArgumentError unless Palindrome.is? number
    @number, @min, @max = number, min, max
  end

  def self.is?(n)
    n.to_s == n.to_s.reverse
  end

  def value
    @number
  end

  def factors
    upper_bound = [Math.sqrt(@number).floor, @max].min
    factors = (@min..upper_bound).
      select {|x| div,mod = @number.divmod(x); mod == 0 and div.between?(@min,@max) }.
      map { |x| [x, @number / x]}
  end
end
