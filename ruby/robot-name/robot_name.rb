class Robot
  attr_reader :name

  LETTER = 'A'..'Z'
  DIGIT = 0..9

  def initialize
    @name = new_name
  end

  def reset
    @name = new_name
  end

  private

  def new_name
    [LETTER, LETTER, DIGIT, DIGIT, DIGIT].map{|range| range.to_a.sample}.join
  end
end
