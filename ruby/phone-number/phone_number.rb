class PhoneNumber
  attr_reader :raw_number

  def initialize(number)
    @raw_number = number
  end

  def number
    digits = @raw_number.delete('^A-Za-z0-9') # well, alphanumerics
    digits = digits[1..-1] if digits.length == 11 and digits[0] == '1'
    digits = '0000000000' if digits.length != 10 or digits.count('^0-9') > 0
    digits
  end

  def area_code
    number[0..2]
  end

  def to_s
    sprintf('(%d) %d-%d', area_code, number[3..5], number[6..9])
  end
end
