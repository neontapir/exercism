class InvalidCodonError < Exception
end

class Translation
  def self.of_rna(strand)
    input = strand
    result = []
    while input.length > 0
      part, remainder = input.slice!(0..2), input
      protein = of_codon(part)
      break if protein == 'STOP'
      result << protein
    end
    result
  end

  def self.of_codon(codon)
    case codon
      when 'AUG'                      then 'Methionine'
      when 'UUU', 'UUC'               then 'Phenylalanine'
      when 'UUA', 'UUG'               then 'Leucine'
      when 'UCU', 'UCC', 'UCA', 'UCG' then 'Serine'
      when 'UAU', 'UAC'               then 'Tyrosine'
      when 'UGU', 'UGC'               then 'Cysteine'
      when 'UGG'                      then 'Tryptophan'
      when 'UAA', 'UAG', 'UGA'        then 'STOP'
      else raise InvalidCodonError
    end
  end
end
