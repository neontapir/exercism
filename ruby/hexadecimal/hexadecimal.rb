class Hexadecimal
  attr_reader :hex

  DIGITS = '0123456789abcdef'.chars

  def initialize(hex)
    hex.downcase!
    hex = '0' if hex.count('^0-9a-f') > 0
    @hex = hex
  end

  def to_decimal
    place = -1
    @hex.reverse.chars.inject(0) do |sum, d|
      place += 1
      sum += DIGITS.index(d) * 16**place
    end
  end
end
