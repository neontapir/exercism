class OCR
  FLAT_DIGITS = {
   '0' => " _ | ||_|",
   '1' => "     |  |",
   '2' => " _  _||_ ",
   '3' => " _  _| _|",
   '4' => "   |_|  |",
   '5' => " _ |_  _|",
   '6' => " _ |_ |_|",
   '7' => " _   |  |",
   '8' => " _ |_||_|",
   '9' => " _ |_| _|",
  }

  CHAR_SIZE = 3

  def initialize(text)
    @text = text
  end

  def convert
    @text.split("\n\n").map { |row| convert_row row }.join(",")
  end

  def convert_row(row)
    to_char_blocks(row).
      inject([]) {|strings, d| strings << d.flatten.join}.
      inject('') {|parsed, s| parsed << parse_symbol(s) }
  end

  def parse_symbol(c)
    FLAT_DIGITS.has_value?(c) ? FLAT_DIGITS.key(c) : '?'
  end

  def to_char_blocks(row)
    row.split("\n").
      map{|s| chunk_into_chars s}.
      map{|a| ensure_not_empty a}.
      map{|a| a.map{|inner| ensure_3_items inner}}.
      transpose
  end

  def chunk_into_chars(string)
    string.chars.each_slice(CHAR_SIZE).to_a
  end

  def ensure_not_empty(array)
    array.empty? ? [[]] : array
  end

  def ensure_3_items(char_array)
    normalized = char_array.join.ljust(CHAR_SIZE).chars
    raise if normalized.length != CHAR_SIZE
    normalized
  end
end
