class Luhn
  def initialize(number)
    @number = number
  end

  def addends
    addends = []
    @number.to_s.chars.
      reverse.map(&:to_i).
      each_with_index{ |digit, index| addends << addend_for(digit, index) }
    addends.reverse
  end

  def checksum
    addends.reduce(&:+)
  end

  def valid?
    checksum % 10 == 0
  end

  def self.append(number, suffix)
    number*10 + suffix
  end

  def self.create(seed)
    check = (0..9).find{|check| new(Luhn.append(seed, check)).valid?}
    Luhn.append(seed, check)
  end

  private

  def addend_for(value, index)
    result = (index%2 == 1 ? value*2 : value)
    result -= 9 if result > 9
    result
  end
end
