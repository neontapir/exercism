class Pangram
  def self.pangram?(phrase)
    phrase.downcase.scan(/\p{Lower}/).uniq.sort.join == ("a".."z").to_a.join
  end
end

module BookKeeping
  VERSION = 5
end
