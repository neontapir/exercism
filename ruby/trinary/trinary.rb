class Trinary
  attr_reader :trinary

  def initialize(trinary)
    trinary = '0' if trinary.count('^0-2') > 0
    @trinary = trinary
  end

  def to_decimal
    place = 0
    @trinary.reverse.chars.inject(0) do |sum, d|
      place += 1
      sum += d.to_i * 3**(place-1)
    end
  end
end
