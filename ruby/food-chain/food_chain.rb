class FoodChain
  VERSION = 2

  def self.song
    self.compose_song ['fly', 'spider', 'bird', 'cat', 'dog', 'goat', 'cow', 'horse']
  end

  private

  def self.compose_song(animals)
    animals.inject('') do |song, a|
      song += get_break(a) + get_beginning(a) + get_commentary(a) \
            + get_swallowing_chain(a, animals)
    end
  end

  def self.get_break(animal)
    animal == 'fly' ? '' : "\n\n"
  end

  def self.get_beginning(animal)
    "I know an old lady who swallowed a #{animal}.\n"
  end

  def self.get_commentary(animal)
    case animal
    when 'cow' then "I don't know how she swallowed a cow!\n"
    when 'goat' then "Just opened her throat and swallowed a goat!\n"
    when 'dog' then "What a hog, to swallow a dog!\n"
    when 'cat' then "Imagine that, to swallow a cat!\n"
    when 'bird' then "How absurd to swallow a bird!\n"
    when 'spider' then "It wriggled and jiggled and tickled inside her.\n"
    else ''
    end
  end

  def self.get_swallowing_chain(animal, animals)
    return "She's dead, of course!\n" if animal == 'horse'

    chain = ""
    chain_until(animal, animals).reverse.each_cons(2) do |eater, eaten|
      eaten = 'spider that wriggled and jiggled and tickled inside her' if eaten == 'spider'
      chain += "She swallowed the #{eater} to catch the #{eaten}.\n"
    end
    chain += "I don't know why she swallowed the fly. Perhaps she'll die."
    chain
  end

  def self.chain_until(animal, animals)
    (animals.take_while{|a| a != animal} or []) << animal
  end
end
