class Simulator
  require_relative 'robot'

  COMMANDS = {
    'A' => :advance,
    'L' => :turn_left,
    'R' => :turn_right
  }

  def instructions(input)
    input.chars.reduce([]) { |cmds, c| cmds << COMMANDS[c] }
  end

  def place(robot, params)
    robot.at(params[:x], params[:y])
    robot.orient params[:direction]
  end

  def evaluate(robot, input)
    input.chars.each { |c| robot.send(COMMANDS[c]) }
  end
end
