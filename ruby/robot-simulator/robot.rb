class Robot
  attr_reader :bearing, :coordinates
  DIRECTIONS = {
    north: [0, 1], east: [1, 0], south: [0, -1], west: [-1, 0]
  }

  def at(x, y)
    @coordinates = [x, y]
  end

  def orient(direction)
    raise ArgumentError unless DIRECTIONS.keys.include? direction
    @bearing = direction
  end

  def turn_left
    turn(-1)
  end

  def turn_right
    turn(1)
  end

  def advance
    @coordinates = vector_add(@coordinates, DIRECTIONS[@bearing])
  end

  private

  def vector_add(a,b)
    a.zip(b).map{|pair| pair.reduce(&:+) }
  end

  def turn(unit)
    new_bearing = (DIRECTIONS.keys.find_index(@bearing) + unit) % DIRECTIONS.keys.length
    orient DIRECTIONS.keys[new_bearing]
  end
end
