class Complement
  VERSION = 3
  NUCLEOTIDE_COMPLEMENTS = {
    'G' => 'C',
    'C' => 'G',
    'T' => 'A',
    'A' => 'U'
  }

  def self.of_dna(dna)
    dna.chars.inject('') do |complement, n|
      complement << of_nucleotide(n)
    end
  end

  def self.of_nucleotide(nucleotide)
    raise ArgumentError unless NUCLEOTIDE_COMPLEMENTS.has_key? nucleotide
    NUCLEOTIDE_COMPLEMENTS[nucleotide]
  end
end
