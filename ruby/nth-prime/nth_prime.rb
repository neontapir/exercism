class Prime
  def self.nth(nth)
    raise ArgumentError if nth < 1

    primes = primes_by_sieve_of_eratosthenes nth

    primes[nth-1]
  end

  private

  def primes_by_sieve_of_eratosthenes(min_needed)
    primes = []
    max = 10

    loop do
      candidates = (2..max).to_a
      for i in 2..max
        if candidates[i-2] then
          j = i*i
          while j <= max do
            candidates[j-2] = 0
            j += i
          end
        end
      end
      primes = candidates.select {|n| n > 0}.to_a
      break if primes.length >= min_needed
      max *= 10
    end

    primes
  end
end
