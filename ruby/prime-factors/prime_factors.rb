module FixnumRefinements
  refine Fixnum do
    def divisible_by?(number)
      self % number == 0
    end
  end
end

class PrimeFactors
  require 'prime'
  using FixnumRefinements

  def self.for(number)
    target = number
    factors = []
    while target > 1
      factors << Prime.each(target).find{ |p| target.divisible_by? p }
      target = target / factors[-1]
    end
    factors
  end
end
