class Crypto
  def initialize(plaintext)
    @plaintext = plaintext
  end

  def normalize_plaintext
    @plaintext.downcase.delete('^a-z0-9')
  end

  def size
    get_size(normalize_plaintext)
  end

  def get_size(text)
    Math.sqrt(text.length).ceil
  end

  def plaintext_segments
    chunks(normalize_plaintext, size)
  end

  def chunks(text, size)
    text.chars.each_slice(size).map(&:join)
  end

  def cipher
    length = plaintext_segments.length
    segments = plaintext_segments.map{|x| x.ljust(length)}
    result = pivot(segments, length)
    result.join
  end

  def ciphertext
    cipher.delete(' ')
  end

  def pivot(source, length)
    result = []
    for row in 0..length
      result[row] = ''
      for column in 0...length
        result[row] << source[column][row] if source[column][row]
      end
    end
    result
  end

  def normalize_ciphertext
    size = Math.sqrt(cipher.length).floor
    chunks(cipher, size).map{|x| x.delete(' ')}.join(' ')
  end
end
