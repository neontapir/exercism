class Triangle
  def initialize(terms)
    @terms = terms
  end

  def rows
    result = [[1]]
    prev = result[0]
    2.upto(@terms) do 
      curr = next_term(prev)
      result << curr
      prev = curr
    end
    result
  end

  def next_term(array)
    result = [1]
    array.each_cons(2) do |a,b|
      result << a+b
    end
    result << 1
  end
end
