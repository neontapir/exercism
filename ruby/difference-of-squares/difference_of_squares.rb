class Squares
  VERSION = 1

  attr_reader :arr
  def initialize(n)
    @arr = digits_array(n)
  end

  def digits_array(n)
    (1..n).to_a
  end

  def square(n)
    n**2
  end

  def sum_of_squares
    @arr.map { |n| square(n) }.reduce(:+)
  end

  def square_of_sums
    square(@arr.reduce(:+))
  end

  def difference
    square_of_sums - sum_of_squares
  end
end
