class Triplet
  def initialize(*sides)
    raise if sides.length != 3
    @sides = sides
  end

  def self.where(constraints)
    min = constraints[:min_factor] || 1
    max = constraints[:max_factor]
    sum = constraints[:sum]

    triplets = []
    (min..max).each do |c|
      (min...c).each do |a|
        b = Math.sqrt(c*c - a*a).to_i
        next if b < a
        triplet = new(a, b, c)
        triplets << triplet if
          triplet.pythagorean? and
          (sum.nil? or triplet.sum == sum)
      end
    end
    triplets
  end

  def sum
    @sides.reduce(&:+)
  end

  def product
    @sides.reduce(&:*)
  end

  def pythagorean?
    a, b, c = @sides
    a*a + b*b == c*c
  end
end
