class Series
  attr_reader :set

  def initialize(numbers)
    @set = numbers.chars.map(&:to_i)
  end

  def slices(size)
    raise ArgumentError if size > set.size
    set.each_cons(size).to_a
  end
end
