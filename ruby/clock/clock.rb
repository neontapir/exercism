class Clock  
  MINUTES_IN_HOUR = 60
  HOURS_IN_DAY = 24

  def self.at(hour, minute = 0)
    Clock.new(hour, minute)
  end

  def +(minutes_to_add)
    hr = (hours + hours(minutes_to_add)) % HOURS_IN_DAY
    min = minutes + minutes(minutes_to_add)
    Clock.new(hr, min)
  end

  def hours(time = @since_midnight)
    time / MINUTES_IN_HOUR
  end

  def minutes(time = @since_midnight)
    time % MINUTES_IN_HOUR
  end

  def -(minutes)
     self + (-1 * minutes)
  end

  def ==(other)
    hours == other.hours && minutes == other.minutes
  end

  def to_s
    sprintf("%2.2d\:%2.2d", hours, minutes)
  end

  private

  def initialize(hour, minute)
    @since_midnight = (hour * MINUTES_IN_HOUR) + minute
  end
end
