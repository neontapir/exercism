class WordProblem
  def initialize(problem)
    @problem = problem
  end

  OPERATIONS = {
    'plus' => lambda {|a,b| a + b},
    'minus' => lambda {|a,b| a - b},
    'multiplied' => lambda {|a,b| a * b},
    'divided' => lambda {|a,b| a / b},
  }

  def answer
    result = nil
    question.each_cons(3).select{|t| is_valid?(t)}.each do |triad|
      operand1, operation, operand2 = triad
      result ||= operand1
      result = OPERATIONS[operation].call result, operand2
    end
    result
  end

  private

  def question
    words = []
    @problem.
      gsub(/(What is|by|\?)/,'').
      split.
      each_with_index{|word,i| words << (i%2==0 ? word.to_i : word)}
    raise ArgumentError, 'Input not recognized as a question' if words.length < 3
    words
  end

  def is_valid?(triad)
    operand1, operation, operand2 = triad
    valid = (operand1.is_a? Numeric and
             operation.is_a? String and
             operand2.is_a? Numeric)
    if valid and not OPERATIONS.keys.include? operation
      raise ArgumentError, "Unknown operation '#{operation}'"
    end
    valid
  end
end
