class Cipher
  attr_reader :key
  def initialize(key = "a"*26)
    raise ArgumentError if key.count('^a-z') > 0 or key.length == 0
    @key = key
  end

  def encode(plaintext)
    transform(lambda {|a,b| a + b}, plaintext)
  end

  def decode(plaintext)
    transform(lambda {|a,b| a - b}, plaintext)
  end

  A_ORD = 'a'.ord
  def transform(operation, plaintext)
    result = ''
    plaintext.chars.each_with_index do |char,index|
      offset = @key[index].ord - A_ORD
      new_pos = operation.call(char.ord - A_ORD, offset) % 26
      result << (new_pos + A_ORD).chr
    end
    result
  end
end
