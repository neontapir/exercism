class Triangle
  def initialize(first, second, third)
    @sides = [first, second, third]
    raise TriangleError unless triangle_inequality?
  end

  def kind
    first, second, third = @sides
    if first == second and second == third
      :equilateral
    elsif first == second or second == third or third == first
      :isosceles
    else
      :scalene
    end
  end

  def triangle_inequality?
    first, second, third = @sides
    (first + second > third and
      second + third > first and
      third + first > second)
  end
end

class TriangleError < ArgumentError
end
