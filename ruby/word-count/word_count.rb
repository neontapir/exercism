class Phrase
  VERSION = 1

  attr_reader :phrase

  def initialize(phrase)
    @phrase = phrase
  end

  def word_count
    counts = Hash.new(0)
    phrase.downcase.scan(/[\w']+/).each do |w|
      w.delete!("'") if w =~ /\'\w+\'/
      counts[w] += 1
    end
    counts
  end
end
