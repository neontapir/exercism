class Say
  def initialize(number)
    @number = number
  end

  ONES = ['', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
  TEENS = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen']
  TENS = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety']
  SCALE = { 9 => 'billion', 6 => 'million', 3 => 'thousand', 2 => 'hundred' }

  def in_english (number = @number)
    raise ArgumentError if number < 0 or number > (10**12 - 1)
    return 'zero' if number == 0

    value = number
    result = []
    while value > 0
      SCALE.each do |power, word|
        place = 10**power
        if value > (place - 1)
          subtrahend = value / place
          result << in_english(subtrahend)
          value -= subtrahend * place
          result << word
        end
      end
      result << parse_small(value) if value > 0
      break
    end
    result.join(' ')
  end

  def parse_small(value)
    if value > 19
      subtrahend = value / 10
      phrase = TENS[subtrahend]
      value -= subtrahend * 10
      phrase += '-' + in_english(value) if value > 0
    elsif value > 9
      phrase = TEENS[value - 10]
    elsif value > 0
      phrase = ONES[value]
    end
    phrase
  end
end
