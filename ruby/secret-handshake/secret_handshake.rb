class SecretHandshake
  ACTIONS = [
    lambda {|h,a| a << 'wink' if h & 1 > 0; a},
    lambda {|h,a| a << 'double blink' if h & 2 > 0; a},
    lambda {|h,a| a << 'close your eyes' if h & 4 > 0; a},
    lambda {|h,a| a << 'jump' if h & 8 > 0; a},
    lambda {|h,a| a.reverse! if h & 16 > 0; a},
  ]

  def initialize(decimal)
    @handshake = (decimal.is_a? Numeric) ? decimal : 0
  end

  def commands
    ACTIONS.inject([]) do |sum, command|
      command.call @handshake, sum
    end
  end
end
