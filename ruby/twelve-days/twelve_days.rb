class TwelveDays
  VERSION = 1

  GIFTS = [
    nil, 'a Partridge in a Pear Tree.', 'two Turtle Doves', 'three French Hens',
    'four Calling Birds', 'five Gold Rings', 'six Geese-a-Laying',
    'seven Swans-a-Swimming', 'eight Maids-a-Milking', 'nine Ladies Dancing',
    'ten Lords-a-Leaping', 'eleven Pipers Piping', 'twelve Drummers Drumming'
  ]

  ORDINAL = [nil, 'first', 'second', 'third', 'fourth', 'fifth', 'sixth',
    'seventh', 'eighth', 'ninth', 'tenth', 'eleventh', 'twelfth' ]

  def self.song
    (1..12).map{ |v| verse(v) }.join("\n\n") + "\n"
  end

  def self.verse(number)
    result = ["On the #{ORDINAL[number]} day of Christmas my true love gave to me"]
    result.concat gifts_for(number)
    result.join(', ')
  end

  def self.gifts_for(number)
    gifts = number.downto(1).map{|n| GIFTS[n]}
    gifts[-1] = "and " + gifts[-1] unless gifts.length == 1
    gifts
  end
end
