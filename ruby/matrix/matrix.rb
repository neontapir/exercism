class Matrix
  def initialize(values)
    @matrix = values.split(/\n/).reduce([]) do |sum, row|
      sum << row.split(' ').map(&:to_i)
    end
  end

  def rows
    @matrix
  end

  def columns
    @matrix.transpose
  end
end
