import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

class RnaTranscription {
    private static final Map<String,String> rnaMap = new HashMap<>();
    static {
        final Map<String,String> m = rnaMap;
        m.put("G", "C");
        m.put("C", "G");
        m.put("T", "A");
        m.put("A", "U");
    }
    
    String transcribe(String dnaStrand) {
        try {
            return Arrays.stream(dnaStrand.split(""))
                .map(x -> rnaMap.get(x).toString())
                .collect(Collectors.joining());
        } catch(NullPointerException e)  {
            throw new IllegalArgumentException("Invalid input");
        }
    }
}
