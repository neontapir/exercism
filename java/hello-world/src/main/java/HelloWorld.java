import java.util.Objects;

public class HelloWorld {
    public static String hello(String input) {
        String name = (input == null || input.equals("")) ? "World" : input;
        return "Hello, " + name + "!";
    }
}
