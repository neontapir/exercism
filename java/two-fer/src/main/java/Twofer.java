public class Twofer {
    public String twofer(String name) {
        String person = name != null ? name : "you";
        return String.format("One for %s, one for me.", person);
    }
}