import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

class LuhnValidator {
    boolean isValid(String candidate) {
        List<Integer> digits = new ArrayList<>();
        for(char ch : candidate.toCharArray()) {
            if (Character.isSpaceChar(ch)) continue;
            if (!Character.isDigit(ch)) {
                return false;
            }
            digits.add(Character.getNumericValue(ch));
        }
        return digits.size() > 1 && (luhnSum(digits) % 10) == 0;
    }

    private int luhnSum(List<Integer> digits) {
        int luhnSum = 0;
        Collections.reverse(digits);
        for (int i = 0; i < digits.size(); i++) {
            int luhnDigit = digits.get(i) * (1 + (i % 2));
            if (luhnDigit > 9) {
                luhnDigit -= 9;
            }
            luhnSum += luhnDigit;
        }
        return luhnSum;
    }
}
