class RaindropConverter {
    String convert(int number) {
        StringBuilder result = new StringBuilder();
        addPhrase(result, number, 3, "Pling");
        addPhrase(result, number, 5, "Plang");
        addPhrase(result, number, 7, "Plong"); 
        addDefault(result, number);
        return result.toString();
    }

    private StringBuilder addPhrase(StringBuilder builder, int number, int multiple, String phrase) {
        if (number % multiple == 0) {
            builder.append(phrase);
        }
        return builder;
    }

    private StringBuilder addDefault(StringBuilder builder, int number) {
        if (builder.length() == 0) {
            builder.append(Integer.toString(number)); 
        }
        return builder;
    }
}
