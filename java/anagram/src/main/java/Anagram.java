import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Anagram {
    private String word;

    public Anagram(String word) {
        this.word = word;
    }

    public List<String> match(List<String> candidates) {
        return candidates.stream().filter(this::isAnagram).collect(Collectors.toList());
    }

    private boolean isAnagram(String candidate) {
        return (! word.equalsIgnoreCase(candidate))
               && Arrays.equals(asLowercaseChars(word), asLowercaseChars(candidate));
    }

    private int[] asLowercaseChars(String string) {
        return string.toLowerCase().chars().sorted().toArray();
    }
}
