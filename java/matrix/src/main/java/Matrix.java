import java.util.List;
import java.util.ArrayList;

class Matrix {
    List<List<Integer>> matrix;

    Matrix(String matrixAsString) {
        matrix = new ArrayList<>();
        for(String i : matrixAsString.split("\\n")) {
            List<Integer> row = new ArrayList<>();
            for(String j : i.split(" ")) {
                row.add(Integer.parseInt(j));
            }
            matrix.add(row);
        }
    }

    int[] getRow(int rowNumber) {
        return matrix.get(rowNumber).stream().mapToInt(i -> i).toArray();
    }

    int[] getColumn(int columnNumber) {
        int[] column = new int[getRowsCount()];
        for(int i = 0; i < getRowsCount(); i++) {
            column[i] = matrix.get(i).get(columnNumber);
        }
        return column;
    }

    int getRowsCount() {
        return matrix.size();
    }

    int getColumnsCount() {
        return matrix.get(0).size();
    }
}
