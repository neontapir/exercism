import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

class Gigasecond {
    private LocalDateTime birthTime;

    Gigasecond(LocalDate birthDate) {
        birthTime = birthDate.atStartOfDay(); 
    }

    Gigasecond(LocalDateTime birthDateTime) {
        birthTime = birthDateTime;
    }

    LocalDateTime getDate() {
        return birthTime.plus((long) 1e9, ChronoUnit.SECONDS);
    }
}
