public class Pangrams {
    public static final String ALPHABET="abcdefghijklmnopqrstuvwxyz";

    public static Boolean isPangram(String sentence) {
        String input = sentence.toLowerCase();
        for(int letter : ALPHABET.chars().toArray()) {
            if (input.indexOf(letter) < 0) {
                return false;
            }
        }
        return true;
    }
}
