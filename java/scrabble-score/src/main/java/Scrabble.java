class Scrabble {
    private char[] word;

    Scrabble(String word) {
        this.word = word.toUpperCase().toCharArray();
    }

    int getScore() {
        int score = 0;
        for (char c : word) {
            score += getLetterScore(c);
        }
        return score;
    }

    int getLetterScore(char c) {
        int score = 0;
        if ("AEIOULNRST".indexOf(c) > -1) {
            score = 1;
        } else if ("DG".indexOf(c) > -1) {
            score = 2;
        } else if ("BCMP".indexOf(c) > -1) {
            score = 3;   
        } else if ("FHVWY".indexOf(c) > -1) {
            score = 4;   
        } else if ('K' == c) {
            score = 5;   
        } else if ("JX".indexOf(c) > -1) {
            score = 8;   
        } else if ("QZ".indexOf(c) > -1) {
            score = 10;   
        }
        return score;
    }
}
