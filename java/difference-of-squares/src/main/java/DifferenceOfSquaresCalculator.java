import java.util.stream.IntStream;

public class DifferenceOfSquaresCalculator {
  public int computeSquareOfSumTo(int value) {
    int sum = IntStream.range(1, value + 1).sum();
    return sum * sum;
  }

  public int computeSumOfSquaresTo(int value) {
    return IntStream.range(1, value + 1).map(i -> i * i).sum();
  }

  public int computeDifferenceOfSquares(int value) {
    return computeSquareOfSumTo(value) - computeSumOfSquaresTo(value);
  }
}