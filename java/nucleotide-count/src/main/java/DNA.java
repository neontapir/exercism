import java.util.HashMap;
import java.util.Map;

public class DNA {
    private final String strand;
    private final String nucleotides = "ACGT";

    public DNA(String strand) {
        this.strand = strand;
    }

    public int count(char value) {
        if (nucleotides.indexOf(value) < 0) throw new IllegalArgumentException();
        int result = 0;
        for(char c : strand.toCharArray()) {
            if (c == value) result++;
        }
        return result;
    }

    public Map<Character, Integer> nucleotideCounts() {
        Map<Character, Integer> result = new HashMap<>();
        for(char n : nucleotides.toCharArray()) {
            result.put(n, 0);
        }
        for(char c : strand.toCharArray()) {
            result.put(c, result.get(c) + 1);
        }
        return result;
    }
}
