import java.util.List;
import java.util.Set;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.stream.IntStream;

class SumOfMultiples {

    private int number;
	private int[] set;

	SumOfMultiples(int number, int[] set) {
        this.number = number;
        this.set = set;
    }

    int getSum() {
        return sum(getMultiples());
    }

	private int sum(Set<Integer> multiples) {
        int sum = 0;
        for (int i : multiples) {
            sum += i;
        }
        return sum;
	}

    private Set<Integer> getMultiples() {
        Set<Integer> multiples = new HashSet<>();
        for (int member : set) {
            int multiple = member;
            while (multiple < number) {
                multiples.add(multiple);
                multiple += member;
            }
        }
        return multiples;
    }
}
