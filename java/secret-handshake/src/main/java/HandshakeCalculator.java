import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class HandshakeCalculator {

    List<Signal> calculateHandshake(int number) {
        List<Signal> result = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            handleSignal(result, number, i);
        }
        return result;
    }

    private void handleSignal(List<Signal> list, int number, int index) {
        if ((number >> index & 1) == 1) {
            if (index < Signal.values().length) {                
                list.add(Signal.values()[index]);            
            } else {                
                Collections.reverse(list);
            }
        }
    }
}