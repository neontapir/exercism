import java.util.HashMap;
import java.util.Map;

public class WordCount {
    public Map<String, Integer> phrase(String input) {
        Map<String, Integer> map = new HashMap<>();
        for(String w : input.toLowerCase().replaceAll("[^a-z0-9 ]", "").split("\\s+")) {
            int oldCount = map.containsKey(w) ? map.get(w) : 0;
            map.put(w, oldCount + 1);
        }
        return map;
    }
}
