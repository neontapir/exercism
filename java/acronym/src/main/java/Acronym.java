class Acronym {
    private char[] phrase;

    Acronym(String phrase) {
        this.phrase = phrase.toCharArray();
    }

    String get() {
        StringBuilder result = new StringBuilder();
        char last = ' ';
        for(int i=0; i<phrase.length; i++) {
            if (Character.isSpaceChar(last) || last == '-') {
                result.append(Character.toUpperCase(phrase[i]));
            }
            last = phrase[i];
        }
        return result.toString();
    }
}
