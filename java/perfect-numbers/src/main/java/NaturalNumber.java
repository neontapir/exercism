import java.util.stream.IntStream;

class NaturalNumber {
  private int number;

  public NaturalNumber(int number) {
    if (number < 1) {
      throw new IllegalArgumentException("You must supply a natural number (positive integer)");
    }
    this.number = number;
  }

  public Classification getClassification() {
    int aliquotSumComparison = ((Integer) getFactors().sum()).compareTo(number);
    return Classification.values()[aliquotSumComparison + 1];
  }

  public IntStream getFactors() {
    int upToSquareRoot = (int) Math.ceil(Math.sqrt(number)) + 1;
    return IntStream.range(1, upToSquareRoot)
      .filter(i -> number % i == 0)
      .flatMap(i -> IntStream.of(i, number/i))
      .distinct()
      .filter(i -> i != number);
  }
}
