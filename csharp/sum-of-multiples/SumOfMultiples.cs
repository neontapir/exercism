﻿using System;
using System.Collections.Generic;
using System.Linq;

public static class SumOfMultiples
{
    public static int Sum(IEnumerable<int> multiples, int max)
    {
        return multiples.SelectMany(x => MultiplesUpTo(x, max)).Distinct().Sum();
    }

    private static IEnumerable<int> MultiplesUpTo(int x, int max) {
        int number = x;
        while (number < max) {
            yield return number;
            number += x;
        }
    }
}