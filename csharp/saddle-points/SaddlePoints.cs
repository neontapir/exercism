using System;
using System.Collections.Generic;
using System.Linq;

public class SaddlePoints
{
    public int[,] Matrix { get; }

    public SaddlePoints(int[,] values)
    {
        Matrix = values;
    }

    public IEnumerable<Tuple<int, int>> Calculate()
    {
        for (int row = 0; row < Matrix.GetLength(0); row++) {
            for (int col = 0; col < Matrix.GetLength(1); col++) {
                if (IsSaddlePoint(Matrix[row, col], row, col)) {
                    yield return Tuple.Create(row, col);
                }
            }
        }
    }

    bool IsSaddlePoint(int point, int row, int col) => GetMatrixRow(row).Max() == point
                                                 && GetMatrixColumn(col).Min() == point;

    IEnumerable<int> GetMatrixRow(int rowNumber) 
    {
        var row = new List<int>();
        for (int i = 0; i < Matrix.GetLength(1); i++) {
            row.Add(Matrix[rowNumber, i]);
        }
        return row;
    }

    IEnumerable<int> GetMatrixColumn(int columnNumber)
    {
        var row = new List<int>();
        for (int i = 0; i < Matrix.GetLength(0); i++) {
            row.Add(Matrix[i, columnNumber]);
        }
        return row;
    }
}