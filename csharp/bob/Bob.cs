using System;
using System.Linq;

public static class Bob
{
    public static string Response(string statement)
    {
        string input = statement.Trim();
        string response = "Whatever.";
        if (input.Length == 0) {
            response = "Fine. Be that way!";
        } else if (input.ToUpper() == input && input.Any(Char.IsLetter)) {
            response = "Whoa, chill out!";
        } else if (input.EndsWith("?", StringComparison.CurrentCulture)) {
            response = "Sure.";
        }
        return response;
    }
}