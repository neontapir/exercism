﻿using System;

public static class CollatzConjecture
{
    public static int Steps(int number)
    {
        if (number < 1) {
            throw new ArgumentException();
        }
        return Collatz(number, 0);
    }

    private static int Collatz(int number, int steps)
    {
        int n = number;
        while (n != 1) {
            if (n % 2 == 0) {
                n /= 2;
            } else {
                n = 3 * n + 1;
            }
            steps++;
        }
        return steps;
    }
}