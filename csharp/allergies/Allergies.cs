using System;
using System.Collections.Generic;

public class Allergies
{
    [Flags]
    public enum Allergens
    {
        Eggs = 1,
        Peanuts = 2,
        Shellfish = 4,
        Strawberries = 8,
        Tomatoes = 16,
        Chocolate = 32,
        Pollen = 64,
        Cats = 128
    }

    public Allergens Mask { get; }

    public Allergies(int mask)
    {
        Mask = (Allergens)(mask % 256);
    }

    public bool IsAllergicTo(string allergy)
    {
        var allergen = Enum.Parse<Allergens>(allergy, ignoreCase: true);
        return (Mask & allergen) > 0;
    }

    public IList<string> List()
    {
        var list = new List<string>();
        if (Mask > 0) {
            list.AddRange(Mask.ToString().ToLower().Split(", "));
        }
        return list;
    }
}