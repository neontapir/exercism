using System;

public struct Clock
{
    private const int HOURS_PER_DAY = 24;
    private const int MINUTES_PER_HOUR = 60;

    public int Hours { get; }
    public int Minutes { get; }

    public Clock(int hours, int minutes)
    {
        //int offset = (int) Math.Floor(minutes / (double) MINUTES_PER_HOUR);
        int offset = minutes / MINUTES_PER_HOUR;
        Hours = Mod(hours + offset, HOURS_PER_DAY);
        Minutes = Mod(minutes, MINUTES_PER_HOUR);
    }

    public Clock Add(int minutesToAdd)
    {
        return new Clock(Hours, Minutes + minutesToAdd);
    }

    public override string ToString()
    {
        return string.Format("{0:00}:{1:00}", Hours, Minutes);
    }

    private static int Mod(int number, int divisor) {
        return (number % divisor + divisor) % divisor;
    }
}