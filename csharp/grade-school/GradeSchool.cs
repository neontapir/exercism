using System;
using System.Collections.Generic;
using System.Linq;

public class School
{
    IDictionary<string, int> studentsByGrade = new Dictionary<string, int>();

    public void Add(string student, int grade)
    {
        studentsByGrade.Add(student, grade);
    }

    public IEnumerable<string> Roster()
    {
        return GetStudents(x => true);
    }

    public IEnumerable<string> Grade(int grade)
    {
        return GetStudents(x => x.Value == grade);
    }

    private IEnumerable<string> GetStudents(Func<KeyValuePair<string, int>, bool> studentsToInclude)
    {
        return studentsByGrade.Where(studentsToInclude)
                     .OrderBy(x => x.Value)
                     .ThenBy(x => x.Key)
                     .Select(x => x.Key);
    }
}