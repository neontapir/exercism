using System;
using System.Collections.Generic;
using System.Linq;

public class NucleotideCount
{
    private const string NUCLEOTIDES = "ACGT";

    public string Sequence { get; }

    public NucleotideCount(string sequence)
    {
        if (!sequence.All(NUCLEOTIDES.Contains)) {
            throw new InvalidNucleotideException();
        }
        Sequence = sequence;
    }

    public IDictionary<char, int> NucleotideCounts
    {
        get
        {
            var tally = NUCLEOTIDES.ToDictionary(c => c, c => 0);
            foreach(char c in Sequence) {
                tally[c]++;
            }
            return tally;
        }
    }
}

public class InvalidNucleotideException : Exception { }
