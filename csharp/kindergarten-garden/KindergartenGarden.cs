﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum Plant
{
    Violets,
    Radishes,
    Clover,
    Grass
}

public class KindergartenGarden
{
    static readonly string[] DEFAULT_STUDENTS = new string[] { 
        "Alice", "Bob", "Charlie", "David", "Eve", "Fred", "Ginny", "Harriet", 
        "Ileana", "Joseph", "Kincaid", "Larry"
    };

    public string[] Garden { get; }
    public string[] Students { get; }

    public KindergartenGarden(string diagram) : this(diagram, DEFAULT_STUDENTS)
    {
    }

    public KindergartenGarden(string diagram, IEnumerable<string> students)
    {
        Garden = diagram.Split();
        Students = students.OrderBy(x => x).ToArray();
    }

    public IEnumerable<Plant> Plants(string student)
    {
        var location = LocationFor(student);
        var plot = StudentPlotBy(location);
        return MapToPlants(plot);
    }

    private int LocationFor(string student)
    {
        return Array.IndexOf(Students, student);
    }

    private IEnumerable<char> StudentPlotBy(int location)
    {
        for (int row = 0; row < 2; row++) {
            int initialColumn = location * 2;
            for (int col = initialColumn; col < initialColumn + 2; col++) {
                yield return Garden[row][col];
            }
        }
    }

    private IEnumerable<Plant> MapToPlants(IEnumerable<char> layout)
    {
        Plant[] plants = (Plant[])Enum.GetValues(typeof(Plant));
        return layout.Select(c => plants.First(p => p.ToString().StartsWith(c)));
    }
}