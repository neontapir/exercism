﻿using System;

public class BinarySearch
{
    public int[] Data { get; }

    const int Min = 0;
    int Max => Data.Length - 1;

    bool ValueIsOutOfBounds(int comparison, int index) =>
        (index == Min && comparison == -1) ||
        (index == Max && comparison == 1);

    int Halfway(int one, int another) => Math.Abs(one - another) / 2;

    public BinarySearch(int[] input)
    {
        Data = input;
    }

    public int Find(int value)
    {
        if (Data.Length == 0) {
            return -1;
        }

        int index = Data.Length / 2;
        int loops = 0;
        int comparison;
        while (true) {
            comparison = value.CompareTo(Data[index]);
            if (comparison == 0) {
                break;
            }
            if (ValueIsOutOfBounds(comparison, index)) {
                index = -1;
                break;
            } 
            index = MoveCloser(comparison, index);

            loops += 1;
            if (loops > Max) {
                index = -1;
                break;
            }
        }

        return index;
    }

    int MoveCloser(int comparison, int index)
    {
        int newIndex = index;
        if (comparison == -1) {
            newIndex = Math.Min(index - 1, Halfway(Min, index));
        } else if (comparison == 1) {
            newIndex = Math.Max(index + 1, Halfway(index, Max));
        }
        return newIndex;
    }
}